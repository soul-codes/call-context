import { context, provide } from "@lib";

test("provide many", () => {
  const c1 = context<number>();
  const c2 = context<boolean>();
  const c3 = context<string>();
  const fn = jest.fn(() => {
    expect(c1.require()).toBe(42);
    expect(c2.require()).toBe(true);
    expect(c3.require()).toBe("hello");
  });

  provide(c1, 42)(c2, true)(c3, "hello")(fn)();
  expect(fn).toHaveBeenCalledTimes(1);
});
