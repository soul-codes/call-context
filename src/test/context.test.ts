import { context } from "@lib";

test("get context", () => {
  const ctx = context<number>();
  const fn = jest.fn(() => {
    expect(ctx.get()).toBe(42);
  });

  expect(ctx.get()).toBeUndefined();
  ctx.provide(42)(fn)();
  expect(ctx.get()).toBeUndefined();
  expect(fn).toHaveBeenCalledTimes(1);
});

test("require context", () => {
  const ctx = context<number>();
  const fn = jest.fn(() => {
    expect(ctx.require()).toBe(42);
  });

  expect(() => ctx.require()).toThrow();
  ctx.provide(42)(fn)();
  expect(() => ctx.require()).toThrow();
  expect(fn).toHaveBeenCalledTimes(1);
});

test("error proof", () => {
  const ctx = context<number>();
  const fn = jest.fn(() => {
    expect(ctx.require()).toBe(42);
  });

  expect(
    ctx.provide(42)(() => {
      throw Error();
    })
  ).toThrow();
  ctx.provide(42)(fn)();
  expect(fn).toHaveBeenCalledTimes(1);
});
