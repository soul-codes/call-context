/**
 * Creates a new context provider/consumer object.
 * @param name
 */
export function context<T>(
  name: string = "(unnamed context)"
): Context<T> & { name: string } {
  let currentValue: { value: T } | null = null;
  const provide = (value: T) => <U extends Function>(fn: U) =>
    ((...args) => {
      let lastValue = currentValue;
      try {
        currentValue = { value };
        return fn(...args);
      } finally {
        currentValue = lastValue;
      }
    }) as U;

  const get = (): T | undefined => (currentValue ? currentValue.value : void 0);

  const require = (): T => {
    if (!currentValue) {
      throw Error(
        `The value for context ${name} is required but not supplied.`
      );
    }
    return currentValue.value;
  };

  return { provide, get, require, name };
}

export interface Context<T> {
  get(): T | undefined;
  require(): T;
  provide: (value: T) => <U extends Function>(fn: U) => U;
}

type Function = (...args: any[]) => any;
