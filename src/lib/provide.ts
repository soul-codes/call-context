import { Context } from "./context";

export const provide = createProvider([]);

function createProvider(provisions: [Context<any>, any][]): Provider {
  function provide<T>(context: Context<T>, value: T): Provider;
  function provide<T extends Function>(fn: T): T;
  function provide(first: Context<any> | Function, second?: any): any {
    if (typeof first === "function") {
      return provisions.reduce(
        (fn, [context, value]) => context.provide(value)(fn),
        first
      );
    } else {
      return createProvider([...provisions, [first, second]]);
    }
  }
  return provide;
}

interface Provider {
  <T>(context: Context<T>, value: T): Provider;
  <T extends Function>(fn: T): T;
}

type Function = (...args: any[]) => any;
